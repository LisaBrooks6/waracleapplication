<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<body>
			<h1>Cakes!</h1>

			<c:choose>
				<c:when test="${empty list}">
					<h2> Cake list is null </h2>
				</c:when>
				<c:otherwise>
				<table>
					<c:forEach items="${list}" var="list">
						<tr>
							<td>Name of cake: <c:out value="${list}.name"/></td>
							<td>Description of cake: <c:out value="${list}.description"/></td>
							<td>Image of cake: <c:out value="${list}.image"/></td>
						</tr>
					</c:forEach>
				</table>
				</c:otherwise>
			</c:choose>		
	</body>
</html>

