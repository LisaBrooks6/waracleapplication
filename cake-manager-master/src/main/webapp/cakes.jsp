<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<body>
			<h1>Cakes!</h1>

			<c:choose>
				<c:when test="${empty list}">
					<h2> Cake list is null </h2>
				</c:when>
				<c:otherwise>
				<table>
					<c:forEach items="${list}" var="list">
						<tr>
							<td>Name of cake: <c:out value="${list}.title"/></td>
							<td>Description of cake: <c:out value="${list}.description"/></td>
							<td>Image of cake: <c:out value="${list}.image"/></td>
						</tr>
					</c:forEach>
				</table>
				</c:otherwise>
			</c:choose>		
			<form action="/cakes" method="POST">
				<table>
					<tr>
						<td>Name of cake:</td>
						<td>
							<input type="text" name="cakeName"/>
						</td>
					</tr>
					<tr>
						<td>Description of cake:</td>
						<td>
							<input type="text" name="cakeDescriptions"/>
						</td>
					</tr>
					<tr>
						<td>Image of cake:</td>
						<td>
							<input type="file" name="cakeImage"/>
						</td>
					</tr>
				</table>
				<button type="button" onclick="location = 'success.jsp'">Add Cake</button>
			</form>
	</body>
</html>	